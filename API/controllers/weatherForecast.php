<?php
class weatherForecast
{
    private $_params;
    public $response = array();

    public function __construct($params)
    {
        $this->_params = $params;
        if( ! isset($this->_params['action']) ) return;

        $this->response['success'] = false;
        $this->response['errormsg'] = "Something went wrong";
        $this->response['result'] = array();

        $this->router();

    }

    public function router() {
        $result = false;
        //Logged action

        if ( $this->_params['action'] == "getMainData" ) {

            $weatherForecast = new weatherForecastModel();
            $result = $weatherForecast->getMainData( isset($this->_params['add_data']) ? $this->_params['add_data'] : null );

        } else if ( $this->_params['action'] == "getWeather" ) {

            $weatherForecast = new weatherForecastModel();
            $result = $weatherForecast->getWeatherDetails( isset($this->_params['spot']) ? $this->_params['spot'] : null, isset($this->_params['add_data']) ? $this->_params['add_data'] : null);

        } else if ( $this->_params['action'] == "userInitial" && isset($this->_params['uuid']) ) {

            $user = new userModel();
            $result = $user->userInit($this->_params['uuid'], (isset($this->_params['settings']) ? $this->_params['settings'] : null) );

        } else if ( $this->_params['action'] == "userUpdate" && isset($this->_params['uuid'], $this->_params['settings']) ) {

            $user = new userModel();
            $result = $user->userUpdate($this->_params['uuid'], $this->_params['settings']);

        }

        if($result) {

            unset($this->response['errormsg']);
            $this->response['success'] = true;
            $this->response['result'] = $result;

        }
    }

}