<?php

session_start();

include_once 'libs/base.php';
include_once 'models/weatherForecastModel.php';
include_once 'models/userModel.php';
include_once 'controllers/weatherForecast.php';

if ( ( isset($argv) && $argv[1] == 'updateWeather' ) || (isset($_GET['test']) && $_GET['test'] == 1 ) ){
    $model = new weatherForecastModel();
    $model->updateData();
    exit;
}

$params = $_REQUEST;

$weatherForecast = new weatherForecast($params);

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
echo json_encode($weatherForecast->response);

exit;