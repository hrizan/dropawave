<?php

class userModel
{

    public function __construct()
    {

    }

    public function userInit($uuid, $settings = null) {
        if($settings && !is_array($settings)) $settings = get_object_vars(json_decode($settings));

        $dbh = Base::getDbInstance();
        try
        {
            $stmt = $dbh->prepare("SELECT * FROM users WHERE device_token = :uuid");
            $stmt->bindParam(':uuid', $uuid, PDO::PARAM_STR);
            $stmt->execute();
            $user = $stmt->fetch(PDO::FETCH_ASSOC);

            if(empty($user)) {

                $user = array('settings' => array(), 'device_token' => $uuid);

                if($settings) $user['settings'] = $settings;

                $stmt = $dbh->prepare("INSERT INTO users (device_token, settings, created ) VALUES (:uuid, :settings, NOW() )");

                $stmt->bindParam(':uuid', $uuid, PDO::PARAM_STR);
                $stmt->bindParam(':settings', serialize($user['settings']), PDO::PARAM_STR);

                $stmt->execute();

                $user['user_id'] = $dbh->lastInsertId();
            } else {

                $user['settings'] = unserialize($user['settings']);
//
//                $user['settings'] = $settings;
//                $this->userUpdate($uuid, $settings);
            }
        }
        catch (Exception $e)
        {
            return array('OK' => 0, 'Error' => 'Something went wrong with the update');
        }

        return $user;
    }

    public function userUpdate($uuid, $settings) {
        if($settings && !is_array($settings)) $settings = get_object_vars(json_decode($settings));

        $settings = serialize($settings);

        $dbh = Base::getDbInstance();
        try {
            $stmt = $dbh->prepare("UPDATE users SET settings = :settings WHERE device_token = :uuid");
            $stmt->bindParam(':uuid', $uuid, PDO::PARAM_STR);
            $stmt->bindParam(':settings', $settings, PDO::PARAM_STR);
            $stmt->execute();
        }
        catch (Exception $e)
        {
            return array('OK' => 0, 'Error' => 'Something went wrong with the update');
        }

        return array('OK' => 1);

    }

}